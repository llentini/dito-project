# README

Projeto com o objetivo de realizar o desafio da Dito.

Para rodar o projeto é necessário:
- comando git clone
- ajustar o database.yml
- comando db:migrate
- comando db:seed

Rotas utilizadas:
- root '/' -> Tabela de usuários com suas pontuações
- '/users/points' -> Tabela dos pontos inseridos através das planilhas
- '/users/spreadsheets' -> Tabela das planilhas copiadas para o diretório

O diretório que o listen está observando é o /{root_project}/public/planilhas, ao colar a planilha csv nesse diretório, o application_controller faz todos os cálculos e atualiza as tabelas no banco de dados. Após copiar, é necessário atualizar a página no browser.
Fiz uma verificação simples de nome da planilha, caso a pessoa exclua e cole uma planilha com o mesmo nome, ela não será considerada (a verificação é feita através da tabela spreadsheets, toda vez que uma planilha é lida, é criada uma row com os dados dela, juntamente com o total de usuários e o total de pontos da mesma).
Na tabela de users, defini dois campos a mais "max_points" que indica se o usuário já chegou ao máximo de pontos limites e também o campo "max_date" que indica a data em que o usuário chegou a pontuação máxima.

Essa foi a primera vez que tive contato com a linguaguem Ruby e com o framework on rails. A maior dificuldade que encontrei foi a sintaxe, como montar uma query, os helpers, até a diferença entre o 'null' para o 'nil'

O que me ajudou foi que o padrão MVC eu já conhecia, pela experiência com o Laravel. Inclusive conceitos de migration, seeds, jobs, etc.
