# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create(name: "Lucas", points: 0, max_points: 0)
User.create(name: "Noel", points: 0, max_points: 0)
User.create(name: "Liam", points: 0, max_points: 0)
User.create(name: "Dave", points: 0, max_points: 0)
User.create(name: "Marina", points: 0, max_points: 0)
User.create(name: "Zoe", points: 0, max_points: 0)
User.create(name: "Emily", points: 0, max_points: 0)
