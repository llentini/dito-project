class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, null:false
      t.float :points, null:false
      t.integer :max_points, null:false, default: 0
      t.date :max_date

      t.timestamps
    end
  end
end
