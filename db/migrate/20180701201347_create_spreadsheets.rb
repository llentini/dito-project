class CreateSpreadsheets < ActiveRecord::Migration[5.2]
  def change
    create_table :spreadsheets do |t|
      t.string :name
      t.integer :total_users
      t.float :total_points

      t.timestamps
    end
  end
end
