class CreatePoints < ActiveRecord::Migration[5.2]
  def change
    create_table :points do |t|
      t.float :points
      t.date :date
      t.references :user, foreign_key: true
      t.references :spreadsheet, foreign_key: true

      t.timestamps
    end
  end
end
