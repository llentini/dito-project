Rails.application.routes.draw do
  root 'users#table'

  get 'users/points', to: 'users#points'

  get 'users/spreadsheets', to: 'users#spreadsheets'
end
