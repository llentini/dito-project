class UsersController < ApplicationController
  def table
    @users = User.all.order(points: :desc)
  end

  def points
    @points = Point.joins(:user).joins(:spreadsheet).all.order(date: :desc)
  end

  def spreadsheets
    @spreadsheets = Spreadsheet.all.order(created_at: :desc)
  end
end
