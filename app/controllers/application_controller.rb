require 'csv'

class ApplicationController < ActionController::Base

  listener = Listen.to('public/planilhas', only: /\.csv$/) do |modified, added, removed|
    #puts "modified absolute path: #{modified}"

    if(added.length > 0)
      for item in added
        splited = item.split('/')
        fileName = splited[splited.length-1]
        #Verifica se o arquivo csv já não foi contabilizado (pelo nome)
        spreadsheet = Spreadsheet.where(name: fileName).take
        if(spreadsheet == nil)
          createdObject = Spreadsheet.create(name: fileName)
          filePath = "public/planilhas/#{fileName}"
          totalPoints = 0;
          totalUsers = 0;
          #Ler o arquivo csv
          CSV.foreach(filePath, headers: true) do |row|
            #date,name,id,points
            userObject = User.find(row[2].to_i)
            #Total de usuario do arquivo csv
            totalUsers = totalUsers + 1
            if(userObject != nil)
              if(userObject.points <= 3126)
                #Total de pontos do arquivo csv
                totalPoints = totalPoints + row[3].to_f
                #Criando os pontos
                Point.create(points: row[3].to_f, user_id: userObject.id, spreadsheet_id: createdObject.id, date:row[0])
                #Atualizar a pontuação do usuario caso esteja abaixo de 3126
                pointsAux = userObject.points + row[3].to_f
                if(pointsAux <= 3126)
                  userObject.update(points: pointsAux)
                else
                  userObject.update(points: 3126, max_points: 1, max_date: row[0])
                end
              end
            end
          end
          createdObject.update({total_points: totalPoints, total_users: totalUsers})
        else
          puts 'Planilha com o mesmo nome já foi adicionada anteriormente'
        end

        puts fileName
        #puts "added absolute path: #{fileName}"
      end
      #redirect_to :root
    end

    if(removed.length > 0)
      puts "removed absolute path: #{removed}"
    end

  end
  listener.start # not blocking

end
